/**
 * @file IO_dijkstra.hpp
 * File that contains the methods used to get inputs from user (files, manual inputs, etc.)
 */

#include "../include/dijkstra.hpp"

#ifndef DIJKSTRA_IO_DIJKSTRA_H
#define DIJKSTRA_IO_DIJKSTRA_H

/**
 * @def DEFINES
 *
 * @{
 */

#define MAX_RAND_NR 100
#define FILE_MAX 8192

/** @} */


extern Dijkstra *input;     /**< Global variable. Here we will store all the relevant data from the maze, starting_node, target_node, etc. */

/**
* Method to get the input from terminal.
* This will get the Dijkstra::nr_nodes and Dijkstra::maze, as inputs from the user, and allocate memory for the @link input @endlink.
* The maze can be generated manually using Dijkstra::Dijkstra(int **v_maze, int v_nr_nodes) or random using Dijkstra::Dijkstra(int v_nr_nodes).
*
* @see Dijkstra::Dijkstra(int **v_maze, int v_nr_nodes)
* @see Dijkstra::Dijkstra(int v_nr_nodes)
*/
void input_by_terminal();

/**
* Method to print the output.
* This method will print the Dijkstra::target_node.Dijkstra_node.dist and Dijkstra::visited (at this point it should have the path from the Dijkstra::starting_node to the target) of the @link input @endlink object.
* @warning This method should be called after Dijkstra::solve_dijkstra()
*
* @see Dijkstra::solve_dijkstra()
*/
void print_dijkstra_output();

/**
* Method to print the input.
* This method will print the Dijkstra::maze of the @link input @endlink object, after generation, to see all the values from the matrix and check if the maze is actually calculated correctly.
*
* @see Dijkstra::maze
*/
void print_dijkstra_input();

/**
* Add the output in a specific file.
* Similar to print_dijkstra_output() but instead of printing on std::cout the messages will be printed in a output file.
*
* @param filename Name of the file in which the output is written.
* @see print_dijkstra_output
*/
int output_to_file(char *filename);

/**
* Get the input from file.
* Similar to input_from_file() but the Dijkstra::starting_node and Dijkstra::target_node will be given when the program is running on std::cin.
*
* @param filename Name of the file from which the input is got.
* @see input_from_file()
*/
int input_from_file_nodes_by_keyboard(char *filename);

/**
* Set the Dijkstra::starting_node and Dijkstra::target_node.
* Is used to set up the values for variables Dijkstra::starting_node and Dijkstra::target_node of @link input @endlink when the program is running using std::cin.
*/
void input_nodes();

#endif //DIJKSTRA_IO_DIJKSTRA_H

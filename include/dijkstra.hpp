/**
 * @file dijkstra.hpp
 * File that contains the declaration of the struct Dijkstra_node and class Dijkstra.
 */

#ifndef DIJKSTRA_DIJKSTRA_H
#define DIJKSTRA_DIJKSTRA_H

/**
 * @def DEFINES
 *
 * @{
 */

#define int_pair pair<int,int>
#define double_pair pair<int_pair,int_pair>

/** @} */

/**
 * Dijkstra_node struct.
 * Is used to store the ROW, COLUMN and DISTANCE of each node in the maze that is being searched.
 */
struct Dijkstra_node {
    int row;    /**< the ROW in the maze */
    int col;    /**< the COLUMN in the maze */
    int dist;   /**< the DISTANCE in the from the starting node to the current node */

    /**
     * Operator < is overwritten for sorting the list of nodes that we should search next. The nodes must be orderd by how close they are to the starting_node.
     *
     * @return this.dist < other.dist;
     */
    bool operator<(const Dijkstra_node &other) const {
        return dist < other.dist;
    }

     bool operator-(const Dijkstra_node &other) const {
        return dist - other.dist;
    }
    /**
     * Operator == is overwritten for checking if 2 nodes are the same. Two nodes are equal when they are on the same ROW and COL.
     *
     * @return this.row == other.row && this.col == other.col;
     */
    bool operator==(const Dijkstra_node &other) const {
        return (row == other.row && col == other.col);
    }

    /**
     * Operator != is overwritten for checking if 2 nodes are not the same. Two nodes are not equal when they are not on the same ROW and COL.
     *
     * @return this.row != other.row && this.col != other.col;
     */
    bool operator!=(const Dijkstra_node &other) const {
        return (row != other.row || col != other.col);
    }
};

/**
 * Dijkstra class.
 * Is used to create the input object.
 */
class Dijkstra {
public:
    int **visited;  /**< The visited nodes. It is used in the algorithm to tell which node was already visited. */
    int **maze;     /**< The maze. The actual maze that will be solved using the DIJKSTRA algorithm. It can be generated randomly or gotten manually.*/
    int nr_nodes;   /**< The size of the maze. Tha maze will be a matrix of size <nr_nodes>x<nr_nodes>.*/

    struct Dijkstra_node starting_node; /**< Starting node in the maze. It contains the coordinates (row and collumn) and the distance is always 0. */
    struct Dijkstra_node target_node;   /**< Target node in the maze. It contains the coordinates (row and collumn), it's distance is assigned at the end of the algorithm. */

    /**
    * Constructor for random generation.
    * This constructor is used when we want to randomly generate the maze.
    *
    * @param v_nr_nodes number of nodes of the matrix. Used to know how much memory to allocate.
    * @see nr_nodes
    * @see alloc_mem()
    */
    Dijkstra(int v_nr_nodes);

    /**
    * Constructor for input from file.
    * This constructor will get from a file the Dijkstra::nr_nodes and Dijkstra::maze.
    * @warning The Dijkstra::starting_node and Dijkstra::target_node are given as parameters when running the program.
    * @param filename Name of the file from which the input is got.
    */
    Dijkstra(char *filename);

    /**
    * Constructor for manual generation.
    * This constructor is used when we want to randomly generate the maze.
    *
    * @param v_maze matrix got as input (file or keyboard).
    * @param v_nr_nodes number of nodes of the matrix. Used to know how much memory to allocate.
    * @see nr_nodes
    * @see maze
    * @see alloc_mem()
    */
    Dijkstra(int **v_maze, int v_nr_nodes);

    /**
    * Destructor.
    * It is also used to delete the maze and visited attributes from the memory at the end of the program.
    *
    * @see maze
    * @see visited
    */
    ~Dijkstra();

    /**
    * Function for solving the maze.
    * This function is called after the nr_nodes, maze, starting_node and target node received values from the user. At the end it will overwrite the Dijkstra::visited attribute to show the path of the solved maze, assign to the Dijkstra::target_node the Dijkstra_node::dist (the cost from start to finish).
    *
    * @see maze
    * @see visited
    * @see starting_node
    * @see target_node
    * @result 1 if the maze can be solved. -1 if the maze can't be solved.
    */
    int solve_dijkstra();

    /**
    * Function for allocating attributes to memory.
    * It calls alloc_mem_matrix() function to allocate the maze and visited attributes to memory
    *
    * @see maze
    * @see visited
    * @see alloc_mem_matrix()
    */
    void alloc_mem();
};

/**
 *
 * Function for allocating variables to memory.
 * Similar to malloc.
 *
 * @param nr_nodes The size that the variables will occupy in the memory.
 * @see Dijkstra::maze
 * @see Dijkstra::visited
 * @see alloc_mem()
 */
int **alloc_mem_matrix(int nr_nodes);

/**
 * Sanity check function.
 * It is used to check if the starting node and target node are valid (they are in the boundries of the maze matrix.
 *
 * @see Dijkstra::starting_node
 * @see Dijkstra::target_node
 */
int are_valid_nodes(int, int, int, int, int);

#endif //DIJKSTRA_DIJKSTRA_H

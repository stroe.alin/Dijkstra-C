current_path=$(pwd)
apt-get update
apt-get install -y build-essential cmake g++ libgtest-dev
if ! test -f /usr/lib/libgtest_main.a && ! test -f /usr/lib/libgtest.a; then
    echo "GTest libraries don't exist\n"
    echo "Starting building the libraries\n"
    cd /usr/src/gtest
    cmake CMakeLists.txt
    make
    cp *.a /usr/lib
fi
cd $current_path

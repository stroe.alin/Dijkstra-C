FROM ubuntu:18.04

MAINTAINER Alin&Mihai <stroe.alin@yahoo.com>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y cmake make gcc g++ build-essential && apt-get clean

RUN apt-get install -y libgtest-dev

WORKDIR /usr/src/gtest
RUN cmake CMakeLists.txt
RUN make

RUN cp /usr/src/gtest/*.a /usr/lib

ADD src /proiect/src/
ADD include /proiect/include/
ADD tests /proiect/tests/
ADD example_io_files/ /proiect/example_io_files/
ADD CMakeLists.txt /proiect/

WORKDIR /proiect

RUN cmake CMakeLists.txt
RUN make

CMD ["./runTests"]

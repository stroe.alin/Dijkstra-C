/**
 * @file IO_dijkstra.cpp
 * File which contains the implementation for methods in IO_dijkstra.hpp
 */

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdbool.h>
#include <string.h>

#include "../include/IO_dijkstra.hpp"
#include "../include/dijkstra.hpp"

using namespace std;

Dijkstra *input;

void input_by_terminal() {
    int **maze;
    int nr_nodes;

    do {
        cout << "Enter size of the maze:" << endl;
        cin >> nr_nodes;
    } while (nr_nodes <= 0);

    maze = alloc_mem_matrix(nr_nodes);

    cout << "Do you want to manually insert the maze or to generate it randomly (M(manual)/R(random) ?" << endl;
    char answer = '0';
    while (answer != 'M' && answer != 'R') {
        cin >> answer;
        if (answer == 'M' || answer == 'R') break;
    }

    if (answer == 'M') {
        cout << "Enter the maze matrix:" << endl;
        for (int i = 0; i < nr_nodes; i++)
            for (int j = 0; j < nr_nodes; j++)
                cin >> maze[i][j];

        input = new Dijkstra(maze, nr_nodes);
    } else {
        input = new Dijkstra(nr_nodes);
    }

    input_nodes();
    delete [] maze;
}


void print_dijkstra_output() {
    cout << "Distance from starting point: row: " << input->starting_node.row << " col: " << input->starting_node.col
         << " to target node: row: " << input->target_node.row << " col: " << input->target_node.col << " is: "
         << input->target_node.dist
         << endl;

    if (input->nr_nodes < 20) {
        for (int i = 0; i < input->nr_nodes; i++) {
            for (int j = 0; j < input->nr_nodes; j++)
                cout << input->visited[i][j] << "\t";
            cout << endl;
        }
    }
}

int input_from_file_nodes_by_keyboard(char *filename) {
    input = new Dijkstra(filename);
    input_nodes();
    return 1;
}

void input_nodes() {
    do {
        cout << "Enter the starting node row:" << endl;
        cin >> input->starting_node.row;
        cout << "Enter the starting node col:" << endl;
        cin >> input->starting_node.col;
    } while (input->starting_node.row >= input->nr_nodes || input->starting_node.col >= input->nr_nodes ||
             input->starting_node.row < 0 ||
             input->starting_node.col < 0);

    do {
        cout << "Enter the target node row:" << endl;
        cin >> input->target_node.row;
        cout << "Enter the target node col:" << endl;
        cin >> input->target_node.col;
    } while (input->target_node.row >= input->nr_nodes || input->target_node.col >= input->nr_nodes ||
             input->target_node.row < 0 || input->target_node.col < 0 ||
             input->target_node.row == input->starting_node.row || input->target_node.col == input->starting_node.col);
}

void print_dijkstra_input() {
    //mark the start and target points with a cost(distance) of 0
    input->maze[input->starting_node.row][input->starting_node.col] = 0;
    input->maze[input->target_node.row][input->target_node.col] = 0;

    if (input->nr_nodes < 20) {
        cout << "Readed maze matrix:" << endl;
        for (int i = 0; i < input->nr_nodes; i++) {
            for (int j = 0; j < input->nr_nodes; j++)
                cout << input->maze[i][j] << "\t";
            cout << endl;
        }

        cout << "\nThe shortest path to find is from node starting_node row: " << input->starting_node.row << " col: "
             << input->starting_node.col << " to target_node row: " << input->target_node.row << " col: "
             << input->target_node.col << endl;
    }
}

int output_to_file(char *filename) {
    strcat(filename, ".txt");
    ofstream f(filename);
    if (!f.is_open()) {
        cout << "Error opening file!\n";
        return -1;
    }

    f << "Distance from starting point: row: " << input->starting_node.row << " col: " << input->starting_node.col
         << " to target node: row: " << input->target_node.row << " col: " << input->target_node.col << " is: "
         << input->target_node.dist
         << endl;

    if (input->nr_nodes < 20) {
        for (int i = 0; i < input->nr_nodes; i++) {
            for (int j = 0; j < input->nr_nodes; j++)
                f << input->visited[i][j] << "\t";
            f << endl;
        }
    }

    filename = nullptr;
    f.close();
    return 1;
}

#include <iostream>
#include <cstdlib>
#include <stdexcept>

#include "../include/dijkstra.hpp"
#include "../include/IO_dijkstra.hpp"


/**
 * @file main.cpp
 */
/**
 * Description of how to run the program.
 * @code
 *      ./program (input and output by terminal). \n
 *      ./program <input_filename> (output by terminal). \n
 *      ./program <input_filename> <output_filename> (starting and target node input by terminal, output to file and image). \n
 *      ./program <input_filename> <output_filename> <start_row> <start_col> <target_row> <target_col>(output to file and image). \n
 * @endcode
 * @warning Please, omit output file extension. It will always be .txt for text and .png for picture.
 * @param argc Number of argumets when we run the program.
 * @param argv The actual arguments when we run the program.
 * @see input_from_file_nodes_by_keyboard()
 * @see input_from_file()
 * @see are_valid_nodes()
 * @see input_by_terminal()
 * @see print_dijkstra_input()
 * @see output_to_file()
 * @see print_dijkstra_output()
 */

int main(int argc, char **argv) {

    int IO_success = 1;

    // INPUT
    argc--; // ignoring filename argument for more visual switch case...
    switch (argc) {
        case 1:
            IO_success = input_from_file_nodes_by_keyboard(argv[1]);
            break;
        case 2:
            IO_success = input_from_file_nodes_by_keyboard(argv[1]);
            break;
        case 6:
            input = new Dijkstra(argv[1]);
            IO_success *= are_valid_nodes(atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), atoi(argv[6]), input->nr_nodes);
            input->starting_node.row = atoi(argv[3]);
            input->starting_node.col = atoi(argv[4]);
            input->target_node.row = atoi(argv[5]);
            input->target_node.col = atoi(argv[6]);
            break;
        default:
            input_by_terminal();
    }

    if (!IO_success) {
        std::cerr<<"Invalid argument"<<std::endl;
        throw std::invalid_argument("Invalid argument exception");
        return -1;
    }

    print_dijkstra_input();

    input->solve_dijkstra();

    // OUTPUT
    switch (argc) {
        case 2:
            IO_success = output_to_file(argv[2]);
            break;
        case 6:
            IO_success = output_to_file(argv[2]);
            break;
        default:
            print_dijkstra_output();
    }

    if (!IO_success) {
        std::cerr<<"Invalid argument"<<std::endl;
       throw std::invalid_argument("Invalid argument exception");
        return -1;
    }

    return 0;
}


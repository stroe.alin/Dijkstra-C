#include "../include/dijkstra.hpp"
#include "../include/IO_dijkstra.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <chrono>
#include <stdexcept>
#include <omp.h>

using namespace std;
using namespace std::chrono;

/**
 * @file dijkstra.cpp
 * File which contains the implementation for methods in dijkstra.hpp
 */

/*
list<struct Dijkstra_node>::reverse_iterator divide_vector(int i, int n, int data_size, list<struct Dijkstra_node> nodes_to_search) {
            list<struct Dijkstra_node>::reverse_iterator it = nodes_to_search.rbegin();
            *it =  data_size * n / i;
    }
*/

int **alloc_mem_matrix(int size) {
    int **a = new int *[size];
    for (int i = 0; i < size; ++i)
        a[i] = new int[size];
    return a;
}

void Dijkstra::alloc_mem() {
    visited = alloc_mem_matrix(nr_nodes);
    maze = alloc_mem_matrix(nr_nodes);
}

Dijkstra::Dijkstra(int v_nr_nodes) {
    nr_nodes = v_nr_nodes;
    alloc_mem();

    srand(time(0));

    for (int i = 0; i < v_nr_nodes; i++)
        for (int j = 0; j < v_nr_nodes; j++) {
            maze[i][j] = rand() % MAX_RAND_NR + 1;
            visited[i][j] = 0;
        }
}

Dijkstra::Dijkstra(char *filename) {
    ifstream f(filename);

    if(!f.good())
    {
        cout<<"\nFile does not exist\n";
         throw exception();
    }

    if (!f.is_open()) {
        cout << ("\nFile opening failed when reading input.\n");
        throw exception();
    } else {
        f >> nr_nodes;
        if (nr_nodes <= 0) {
            cout << "The number of nodes read from the file is not valid";
            throw exception();
        } else {

            alloc_mem();

            for (int i = 0; i < nr_nodes; i++)
                for (int j = 0; j < nr_nodes; j++) {
                    f >> maze[i][j];
                    visited[i][j] = 0;
                }

            target_node.dist = 0;
            starting_node.dist = 0;

            f.close();
            filename = nullptr;
        }
    }
}

Dijkstra::Dijkstra(int **v_maze, int v_nr_nodes) {
    nr_nodes = v_nr_nodes;
    alloc_mem();

    for (int i = 0; i < v_nr_nodes; i++)
        for (int j = 0; j < v_nr_nodes; j++) {
            maze[i][j] = v_maze[i][j];
            visited[i][j] = 0;
        }
}

Dijkstra::~Dijkstra() {
    delete[] maze, visited;
}

int Dijkstra::solve_dijkstra() {
    map<int_pair, int_pair> parents;

    target_node.dist = 0;
    starting_node.dist = 0;
    if(!are_valid_nodes(starting_node.row, starting_node.col, target_node.row, target_node.col, nr_nodes)) {return -1;}
    
    maze[starting_node.row][starting_node.col] = 0;
    maze[target_node.row][target_node.col] = 0;

    // applying Dijkstra on matrix cells starting from source
    vector<struct Dijkstra_node> nodes_to_search;

    nodes_to_search.push_back(starting_node);

    visited[starting_node.row][starting_node.col] = 1;
    struct Dijkstra_node current_node;

    if (starting_node.row == target_node.row and starting_node.col == target_node.col) { return 1; };

    duration<double> aux_chrono;
    while (!nodes_to_search.empty()) {

        auto start_chrono = high_resolution_clock::now(); 
        int data_size = nodes_to_search.size();
        auto divide_vector = [data_size](int idx, int num_threads) {
              return data_size * idx / num_threads;
       };
        int num_threads = 0; 

        #pragma omp parallel num_threads (2)
        {
                  num_threads = omp_get_num_threads();
                  int idx = omp_get_thread_num();
                  int start_block = divide_vector( idx, num_threads );
                  int finish_block = divide_vector( idx+1, num_threads );
                  auto start_it = nodes_to_search.begin();
                  sort(start_it+start_block, start_it+finish_block);  
        //always need to get the point that is closest to the source, otherwise the minumum distance might not be found.
        }

        //this merge works only with an even number of threads
        for (int merge_step = 1; merge_step < num_threads; merge_step *= 2 )
        {
            #pragma omp parallel for num_threads(2)
            for (int i = 0; i < (num_threads/2/merge_step); ++i) {
            int start = divide_vector(i*2*merge_step, num_threads);
            int mid = divide_vector(i*2*merge_step+merge_step, num_threads);
            int finish = divide_vector(i*2*merge_step+2*merge_step, num_threads);
            finish = std::min(finish, data_size);
            auto b = std::begin(nodes_to_search);
            std::inplace_merge( b+start, b+mid, b+finish );
          }
        }  


        auto stop_chrono = high_resolution_clock::now();
        current_node = nodes_to_search[0];

        aux_chrono += stop_chrono - start_chrono;
        nodes_to_search.erase(nodes_to_search.begin());

        struct Dijkstra_node neighbour;

        //Search all all neighbours (in this case all possible moves UP, DOWN, LEFT, RIGHT
        //down
        if ((current_node.row + 1 < nr_nodes) && visited[current_node.row + 1][current_node.col] == 0) {
            neighbour.dist = current_node.dist + maze[current_node.row + 1][current_node.col];
            neighbour.row = current_node.row + 1;
            neighbour.col = current_node.col;
            nodes_to_search.push_back(neighbour);
            visited[current_node.row + 1][current_node.col] = 1;
            parents.insert(
                    double_pair(int_pair(neighbour.row, neighbour.col), int_pair(current_node.row, current_node.col)));
        }

        //up
        if ((current_node.row - 1 >= 0) && visited[current_node.row - 1][current_node.col] == 0) {
            neighbour.dist = current_node.dist + maze[current_node.row - 1][current_node.col];
            neighbour.row = current_node.row - 1;
            neighbour.col = current_node.col;
            nodes_to_search.push_back(neighbour);
            visited[current_node.row - 1][current_node.col] = 1;
            parents.insert(
                    double_pair(int_pair(neighbour.row, neighbour.col), int_pair(current_node.row, current_node.col)));
        }

        //left
        if ((current_node.col - 1 >= 0) && visited[current_node.row][current_node.col - 1] == 0) {
            neighbour.dist = current_node.dist + maze[current_node.row][current_node.col - 1];
            neighbour.row = current_node.row;
            neighbour.col = current_node.col - 1;
            nodes_to_search.push_back(neighbour);
            visited[current_node.row][current_node.col - 1] = 1;
            parents.insert(
                    double_pair(int_pair(neighbour.row, neighbour.col), int_pair(current_node.row, current_node.col)));
        }

        //right
        if ((current_node.col + 1 < nr_nodes) && visited[current_node.row][current_node.col + 1] == 0) {
            neighbour.dist = current_node.dist + maze[current_node.row][current_node.col + 1];
            neighbour.row = current_node.row;
            neighbour.col = current_node.col + 1;
            nodes_to_search.push_back(neighbour);
            visited[current_node.row][current_node.col + 1] = 1;
            parents.insert(
                    double_pair(int_pair(neighbour.row, neighbour.col), int_pair(current_node.row, current_node.col)));
        }

        if (current_node.row == target_node.row && current_node.col == target_node.col) {

            for (int i = 0; i < nr_nodes; i++)
                for (int j = 0; j < nr_nodes; j++)
                    visited[i][j] = 0;
            do {
                visited[current_node.row][current_node.col] = 1;
                int_pair aux = parents[int_pair(current_node.row, current_node.col)];
                current_node.row = aux.first;
                current_node.col = aux.second;
            } while (current_node.row != starting_node.row || current_node.col != starting_node.col);

            visited[starting_node.row][starting_node.col] = 1;

            target_node.dist = current_node.dist;

            cout << "Total sorting time: " << aux_chrono.count() << " s" << endl;
            return 1;
        }
    }
    return -1;
}

int are_valid_nodes(int start_row, int start_col, int target_row, int target_col, int max) {
    int valid = 1;
    if (start_col < 0 || start_row < 0 || target_col < 0 || target_row < 0)
        valid = 0;
    if (start_col >= max || start_row >= max || target_col >= max || target_row >= max)
        valid = 0;
    return valid;
}

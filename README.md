# Dijkstra C++
Dijkstra algorithm implementation in C++. Provides the possibility of reading the a maze from file input, and generate another file as output; or reading and printing via terminal.
It also includes an array of unit tests written using Gtest framework and coverage report

[Algorithm description](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm).
## Current features
- Dijkstra shortest path algorithm implementation.
- Supports both input and ouput via terminal and files.
- Supports files representing the maze as a matrix of (NR_NODES x NR_NODES).
- Supports differents usages, one of them non verbosing, ideal for repeating program execution in a set of files.
- Applies segregations of concerns, separating logic from algorithm and from input/output through 2 main public interfaces:
    - `dijkstra.cpp`, responsible of algorithm calculation. Completely reusable.
    - `IO_dijkstra.cpp`, utilities for reading algorithm input via keyboard and printing output to terminal or files.
- Google test integration for unit-testing
- Coverage report using gcovr
- Documentation generated using Doxygen

## Compiling
Project contains a CMakeList.txt file.

Generate the CMake files:
`cmake CMakeLists.txt`
Build the project using:
`make`
It will generate two executable files:
 - Dijkstra - for running the program 
 - runTests - running the unit-tests

Project contains an Dockerfile so you can build it and run it indepedent from host OS
Build the image using the following command in project directory
`docker image build . -t <desired_tag>`

Run the Docker image using:
`docker run <desired_tag>`

## Usages
    ./program                                             // input and output by terminal <br>
    ./program <input_filename>                            // output by terminal <br>
    ./program <input_filename> <output_filename>          // starting and target node input by terminal <br>
    ./program <input_filename> <output_filename> <starting_node> <target_node> <br>

An example of <input_filename> can be found in `example_io_files/test_file.in` 
    
#### Authors
Neacsu Mihai, Alin Stroe

#### License
[The Unlicense](http://unlicense.org/). Software released into the public domain, no rights reserved.


/**
 * @file test_dijkstra.cpp
 * File that contains the definitions of the tests used.
 */
#include <gtest/gtest.h>
#include <iostream>
#include <cstring>
#include <string>
#include <cstdlib>

#include "../include/dijkstra.hpp"
#include "../include/IO_dijkstra.hpp"

/**
 * @defgroup TESTS1 Basic
 *
 * @{
 */

TEST(basic_tests, basic_small_maze_test)
{
    Dijkstra *object = new Dijkstra(5);
    object->starting_node.row = 0;
    object->starting_node.col = 0;
    object->target_node.row = 4;
    object->target_node.col = 4;

    ASSERT_EQ(object->solve_dijkstra(), 1);
    delete object;
}


TEST(basic_tests, maze_constructor_test)
{
    
    int **test_maze = alloc_mem_matrix(2);
    for (int i = 0;i<2;i ++)
        for (int j = 0;j<2;j++)
            test_maze[i][j] = i;
    
    Dijkstra *object = new Dijkstra(test_maze,2);
    object->starting_node.row = 0;
    object->starting_node.col = 0;
    object->target_node.row = 1;
    object->target_node.col = 0;

    ASSERT_EQ(object->solve_dijkstra(), 1);
    delete object;
}


TEST(basic_tests, same_start_and_target_test)
{
    Dijkstra *object = new Dijkstra(5);
    object->starting_node.row = 2;
    object->starting_node.col = 2;
    object->target_node.row = 2;
    object->target_node.col = 2;

    ASSERT_EQ(object->solve_dijkstra(), 1);
    delete object;
}

TEST(basic_tests, unsolvable_maze)
{
    Dijkstra *object = new Dijkstra(18);
    object->starting_node.row = 0;
    object->starting_node.col = 0;
    object->target_node.row = 19;
    object->target_node.col = 19;

    ASSERT_EQ(object->solve_dijkstra(), -1);
    delete object;
}

TEST(basic_tests, distance_test)
{
    std::string str = "example_io_files/test_file.in";
    Dijkstra *object = new Dijkstra(const_cast<char*>(str.c_str()));

    object->starting_node.row = 0;
    object->starting_node.col = 0;
    object->target_node.row = 19;
    object->target_node.col = 19;
    object->solve_dijkstra();

    ASSERT_EQ(object->target_node.dist, 1015);
    delete object;
}


TEST(basic_tests, inexistent_file)
{
    std::string str = "example_io_files/test_file_not.in";
    ASSERT_THROW(new Dijkstra(const_cast<char*>(str.c_str())), std::exception);

}

/** @} */

/**
 * @defgroup TESTS2 Random
 *
 * @{
 */

TEST(random_tests, random_small_maze_test)
{
    srand(time(0));
    Dijkstra *object = new Dijkstra(10);
    object->starting_node.row = rand() % 10;
    object->starting_node.col = rand() % 10;
    object->target_node.row = rand() % 10;
    object->target_node.col = rand() % 10;

    ASSERT_EQ(object->solve_dijkstra(), 1);
    delete object;
}




TEST(random_tests, medium_maze_test)
{
    srand(time(0));

    Dijkstra *object = new Dijkstra(200);
    object->starting_node.row = rand() % 200;
    object->starting_node.col = rand() % 200;
    object->target_node.row = rand() % 200;
    object->target_node.col = rand() % 200;

    ASSERT_EQ(object->solve_dijkstra(), 1);
    delete object;
}

TEST(random_tests, large_maze_test)
{
    srand(time(0));

    Dijkstra *object = new Dijkstra(500);
    object->starting_node.row = rand() % 500;
    object->starting_node.col = rand() % 500;
    object->target_node.row = rand() % 500;
    object->target_node.col = rand() % 500;

    ASSERT_EQ(object->solve_dijkstra(), 1);
}

/** @} */

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

var searchData=
[
  ['alloc_5fmem_44',['alloc_mem',['../classDijkstra.html#aeb48f10e0a5d100eadde3084c1055b9f',1,'Dijkstra']]],
  ['alloc_5fmem_5fmatrix_45',['alloc_mem_matrix',['../dijkstra_8cpp.html#a548cc8f638634e2b33f6121422f48e3d',1,'alloc_mem_matrix(int size):&#160;dijkstra.cpp'],['../dijkstra_8hpp.html#ad8adb627af69a5ca371972a3c7e4f598',1,'alloc_mem_matrix(int nr_nodes):&#160;dijkstra.cpp']]],
  ['are_5fvalid_5fnodes_46',['are_valid_nodes',['../dijkstra_8cpp.html#aa7a3565b6f1bd2d9160f36f88e84a572',1,'are_valid_nodes(int start_row, int start_col, int target_row, int target_col, int max):&#160;dijkstra.cpp'],['../dijkstra_8hpp.html#a8785d51320fbcb9c339b45237444ced9',1,'are_valid_nodes(int, int, int, int, int):&#160;dijkstra.cpp']]]
];
